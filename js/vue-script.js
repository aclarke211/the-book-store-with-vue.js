var googleBooksURL = "https://www.googleapis.com/books/v1/volumes?q=HTML5";
var errorMessage = "There was a problem retrieving books from Google Books API."

var app = new Vue({
    el: '#app',
    data: {
        mainBooks: [],
        featuredBooks: [],
        selectedBooks: []
    },
    filters: {
        truncateDescription: (longDescription) => {
            shortDescription = longDescription.substring(0, 140) + "...";
            return shortDescription;
        }
    },
    methods: {
        findResults: function() {
            // ===========================================================================================================================
            // Axios method (improved compatability over fetch)
            // For Axios to work add following to index.html --> <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
            // axios.get(googleBooksURL).then(response=>{var allBooks=response.data.items.map(book=>{book.checked=false;if(this.selectedBooks.includes(book.id)){book.checked=true;} console.log(book);return book;});this.mainBooks=allBooks;this.featuredBooks=allBooks.splice(-2);}).catch(error=>{alert(errorMessage);});
            // ===========================================================================================================================
           
            fetch(googleBooksURL)
                .then(
                    response => {
                        if (response.status !== 200) {
                            console.log(`Looks like there was a problem. Status Code: ${response.status}`);
                            return;
                        }
                        response.json().then(data => {
                            var allBooks = data.items.map(book => {
                                book.checked = false;
                                if (this.selectedBooks.includes(book.id)) {
                                    book.checked = true;
                                }
                                return book;
                            });
                            this.mainBooks = allBooks;
                            this.featuredBooks = allBooks.splice(-2);
                        });
                    }
                )
                .catch(err => {
                    console.log(errorMessage, err);
                });
        },
        checkForSelectedBooks: function(book) {
            // console.log("Book: " + book.id + " (" + book.volumeInfo.title + ") " + "| Clicked.");

            if (this.selectedBooks.includes(book.id)) {

                for (var i = 0; i < this.selectedBooks.length; i++) {
                    if (this.selectedBooks[i] == book.id) {
                        this.selectedBooks.splice(i, 1);
                    }
                }
            } else {
                this.selectedBooks.push(book.id);
            }
            book.checked = !book.checked;
            this.set_data();
        },
        set_data: function() {
            localStorage.setItem('storedSelectedBooks', JSON.stringify(this.selectedBooks));
        },
        get_data: function() {
            return JSON.parse(localStorage.getItem('storedSelectedBooks')) || [];
        }
    },
    mounted() {
        this.selectedBooks = this.get_data();
        this.findResults();
    }
})